﻿module FileUpdaters
    open System.IO
    open System

    let getFilesFromDirectory directory =
        match Directory.Exists directory with
        | true -> 
            try
                Directory.GetFiles(directory, "*AssemblyInfo.cs", SearchOption.AllDirectories) |> Array.toList |> Option.Some
            with
                | Exception as ex -> 
                    printfn "Couldn't get files from %s\nError Message:%s" directory ex.Message
                    None
        | false -> None

    let readFileContents (fileName:string) =
        match File.Exists(fileName) with
        | true ->  
            try 
                use lineReader = new StreamReader(fileName)
                lineReader.ReadToEnd() |> Option.Some
            with
                | Exception as ex -> 
                    printfn "Couldn't open file %s" fileName
                    None
        | false -> None

    let replaceContent (version:string) (content:string option) =
        let replaceToken (startToken:string) (versionToken:string) (fileContent:string option) =
            match fileContent with
            | None -> None
            | Some content -> 
                match content.Contains(startToken) with
                | false -> Some content
                | true -> 
                    let startIndex = content.LastIndexOf(startToken)
                    let endIndex = content.IndexOf("]", startIndex)
                    let oldPiece = content.Substring(startIndex, endIndex-startIndex)
                    content.Replace(oldPiece, startToken + versionToken + "\")") |> Option.Some

        let assemblyVersion = "[assembly: AssemblyVersion(\""
        let fileVersion = "[assembly: AssemblyFileVersion(\""

        content |> replaceToken assemblyVersion version |> replaceToken fileVersion version

    let saveChanges (location:string) (content:string option) =
        match content with
        | Some content -> 
            use writer = new StreamWriter(location)
            writer.WriteLine content
        | None -> ()

    let setVersionForDirectory version directory =
        match directory |> getFilesFromDirectory with
        | Some files -> 
            printfn "Updating files."
            files |> List.iter (fun x -> readFileContents x |> replaceContent version |> saveChanges x)
            printfn "Finished updating files."
        | None -> printfn "Couldn't update files."
