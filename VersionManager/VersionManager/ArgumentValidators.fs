﻿module ArgumentValidators
    open Models
    open System
    open System.IO

    let validateVersion (version:string) =
        let validateVersionNumber number =
            match number |> Int32.TryParse with
            | true, value -> if value > -1 && value <= 65534 then Some number else None
            | false, _ -> if number = "*" then Some number else None

        match version.ToCharArray() |> Array.filter (fun x -> x = '.') |> Array.length with
        | 1 -> None
        | 2 | 3 | 4 ->
            let validateVersionPices = version.Split([|'.'|], StringSplitOptions.RemoveEmptyEntries) |> Array.map(fun x -> validateVersionNumber x)
            let isTheVersionValid = validateVersionPices|> Array.filter (fun y -> y = None) |> Array.isEmpty
            if isTheVersionValid then Some version else None
        | _ -> None

    let validateDirectory directory =
        match directory |> Directory.Exists with
        | true -> Some directory
        | false -> None

    let validateArguments (argument:Arguments) =
        match validateVersion argument.version with
        | None -> 
            printfn "The specified version: %s is not valid" argument.version
            None
        | Some _ -> 
            match argument.directory |> validateDirectory  with
            | None -> 
                printfn "The specified directory: %s does not exist" argument.directory
                None
            | Some x -> Some argument
