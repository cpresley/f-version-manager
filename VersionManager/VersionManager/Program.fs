﻿open System.IO
open System
open ArgumentParsers
open FileUpdaters
open ArgumentValidators

[<EntryPoint>]
let main argv = 
    let usageMessage = sprintf @"Example usage -> VersionManager.exe version=2.1.0 directory=C:\temp\"
    match parseArguments argv with
    | None -> printfn "Incorrect arguments specified.\n%s" usageMessage
    | Some argument -> 
        match validateArguments argument with
        | Some _ -> 
            printfn "version %s and directory %s" argument.version argument.directory
            setVersionForDirectory argument.version argument.directory
        | _ -> printfn "Invalid arguments"
    0
