﻿module ArgumentParsers
open Models

let private findArgument (argv:string array) (argName:string) =
    match argv |> Array.tryFind(fun x -> x.StartsWith(argName)) with
    | Some arg -> arg.Substring(argName.Length) |> Option.Some
    | None -> None

let private findVersion (argv:string array) = findArgument argv "version="
let private findDirectory (argv:string array) = findArgument argv "directory="

let parseArguments argv =
    match findVersion argv, findDirectory argv with
    | Some version, Some directory -> Some {version=version; directory=directory}
    | _ -> None
