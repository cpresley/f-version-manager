﻿module When_updating_a_string

open NUnit.Framework
open Swensen.Unquote
open FileUpdaters

[<Test>]
let ``And the content is None, then None is returned`` () =
    test <@ replaceContent "2.1.0" None = None @>

[<Test>]
let ``And the content is "Content", then Some "Content" is returned `` () =
    test <@ replaceContent "2.1.0" (Some "Content") = (Some "Content") @>

[<Test>]
let ``And the content is "[assembly: AssemblyVersion("1.0.0")]" and the new version 2.0.0, then Some "[assembly: AssemblyVersion("2.0.0")]" is returned`` () =
    test <@ replaceContent "2.0.0" (Some "[assembly: AssemblyVersion(\"1.0.0\")]") = Some "[assembly: AssemblyVersion(\"2.0.0\")]" @>