﻿namespace VersionManager.Tests
module When_parsing_arguments =
    open NUnit.Framework
    open Swensen.Unquote
    open ArgumentParsers

    [<Test>]
    let ``And the list of arguments is empty, then None,None is returned`` () = 
        test<@ parseArguments [||] = None @>

    [<Test>]
    let ``And the list of arguments don't contain version or directory, then None,None is returned`` () =
        test <@ parseArguments [|"someThing="; "someOtherThing="|] = None @>
    [<Test>]
    let ``And the list of arguments only includes version="2.0.1" then None is returned`` () =
        test<@ parseArguments [|"version=2.0.1"|] = None @>

    [<Test>]
    let ``And the list of arguments only includes directory="someDirectory" then None is returned`` () =
        test <@ parseArguments [|"directory=someDirectory"|] = None @>

    [<Test>]
    let ``And the list of arguments is version="2.0.1" and directory="someDirectory", then Some "2.0.1",Some "someDirectory" is returned`` () =
        test <@ parseArguments [|"version=2.0.1"; "directory=someDirectory"|] = Some {version="2.0.1";directory="someDirectory"} @>

    [<Test>]
    let ``And the list contains more arguments that needed, then the extra arguments are ignored`` () =
        test<@ parseArguments [|"version=2.0.1"; "notNeeded="; "directory=someDirectory"|] = Some {version="2.0.1";directory="someDirectory"}@>
