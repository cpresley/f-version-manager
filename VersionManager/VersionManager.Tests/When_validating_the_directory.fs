﻿module When_validating_the_directory
    open NUnit.Framework
    open Swensen.Unquote
    open ArgumentValidators

    [<Test>]
    let ``And the directory is empty, then None is returned`` () =
        test <@ validateDirectory "" = None @>

    [<Test>]
    let ``And the directory does not exist, then None is returned`` () =
        test <@ validateDirectory "someDirectory" = None @>

    [<Test>]
    let ``And the directory exists, then Some directory is returned`` () =
        test <@ validateDirectory "C:\\" = Some "C:\\" @>
