﻿module When_saving_changes

open NUnit.Framework
open Swensen.Unquote
open System.IO
open FileUpdaters

let readContent (filename:string) = 
        use reader = new StreamReader(filename)
        reader.ReadToEnd().Trim()

[<Test>]
let ``And the file does not exist, then the file is created with the content`` () =
    File.Delete("tempFile.txt")
    
    saveChanges "tempFile.txt" (Some "Some file content")
    
    test <@ readContent "tempFile.txt" = "Some file content" @>

[<Test>]
let ``And the file does exist, then the contents are overwritten`` () =
    File.Delete("anotherFile.txt")
    use writer = new StreamWriter("anotherFile.txt")
    writer.WriteLine("content")
    writer.Close()

    saveChanges "anotherFile.txt" (Some "Some more content")

    test <@ readContent "anotherFile.txt" = "Some more content" @>