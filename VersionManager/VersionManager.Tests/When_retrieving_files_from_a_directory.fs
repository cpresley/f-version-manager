﻿module When_retrieving_files_from_a_directory

open NUnit.Framework
open Swensen.Unquote
open FileUpdaters
open System.IO

[<Literal>]
let directory = "directory"

let prepareDirectory () =
    if Directory.Exists(directory) then Directory.Delete(directory, true)
    Directory.CreateDirectory(directory).Create()

let createFile name = 
    File.Create(directory + "\\" + name).Close()

[<Test>]
let ``And the directory does not exist, then None is returned`` () =
    test <@ getFilesFromDirectory "someMadeUpDirectory" = None @>

[<Test>]
let ``And the directory does not have any AssemblyInfo files, then None is returned`` () =
    prepareDirectory()
    createFile "someFile.txt"
    test <@ getFilesFromDirectory directory = Some [] @>

[<Test>]
let ``And the directory has two files, then Some (those two files) are returned`` () =
    prepareDirectory ()
    createFile "AssemblyInfo.cs"
    createFile "AnotherAssemblyInfo.cs"

    test <@ getFilesFromDirectory directory = Some [ directory+"\\AnotherAssemblyInfo.cs"; directory+"\\AssemblyInfo.cs";] @>


