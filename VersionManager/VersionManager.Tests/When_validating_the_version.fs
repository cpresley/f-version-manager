﻿module When_validating_the_version
    open NUnit.Framework
    open Swensen.Unquote
    open ArgumentValidators

    [<Test>]
    let ``And the version is 2.0.1, then 2.0.1 is returned`` () =
        test<@ validateVersion "2.0.1" = Some "2.0.1" @>

    [<Test>]
    let ``And the version is 2.0.*, then 2.0.* is returned`` () =
        test <@ validateVersion "2.0.*" = Some "2.0.*" @>

    [<Test>]
    let ``And the version is 2, then None is returned`` () =
        test <@validateVersion "2" = None @>

    [<Test>]
    let ``And the version is 2.0, then None is returned`` () =
        test <@validateVersion "2.0" = None @>

    [<Test>]
    let ``And the version is 2.1.0, then Some 2.1.0 is returned`` () =
        test <@validateVersion "2.1.0" = Some "2.1.0" @>

    [<Test>]
    let ``And the version is 2.abc.1.0, then None is returned`` () =
        test <@validateVersion "2.abc.1.0" = None @>

    [<Test>]
    let ``And the version is -1.0.0.0, then None is returned`` () =
        test <@validateVersion "-1.0.0.0" = None @>

    [<Test>]
    let ``And the version is 2.1.0.(the maximum number supported), then Some version is returned`` () =
        test <@validateVersion "2.1.0.65534" = Some "2.1.0.65534" @>

    [<Test>]
    let ``And the version is 2.1.0.(one more than max supported), then None is returned`` () =
        test <@validateVersion "2.1.0.65535" = None @>
