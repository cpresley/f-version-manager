﻿module When_reading_the_file_contents

open NUnit.Framework
open Swensen.Unquote
open FileUpdaters
open System.IO

let writeFile (location:string) (content:string) =
    use writer = new StreamWriter(location)
    writer.WriteLine(content)
    writer.Close()


[<Test>]
let ``And the file does not exist, then None is returned`` () =
    test <@ readFileContents "doesNotExist.txt" = None @>

[<Test>]
let ``Then the contents are returned`` () =
    File.Delete("someFile.txt")
    writeFile "someFile.txt" "Here's some content"

    test <@ readFileContents "someFile.txt" = Some ("Here's some content\r\n") @>

